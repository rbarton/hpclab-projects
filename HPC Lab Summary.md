# HPC Lab Summary

## Project 1

- memory hierarchy, lscpu
- associativity, strides, locality (temporal and spacial)
- matmul example
  1. look ordering, blocking
  1. extend with openmp
  1. blocking, how to choose size
  1. avx
  1. handling of edge cases
  - complexity O(n^3)
  - comparison to mkl blas
  - benchmarking & plotting
- theoretical peak perf  -> (parallel) efficiency
- blas, level 1 = vector ops (dot-prod, cwise add), level 2 = mat-vec ops (gemv, lse eval), level 3 = mat-mat ops (mat-mul/gemm)
- euler usage, interactive job

## Project 2



## Project 7

- Jupyter and python
- Poisson equation
  - Dirichlet BDC
  - 2nd order finite-difference discr
  - -> non linear programming
  - 'full-space' approach treats both control and state vars as optimizations vars -> gives high dim nonlinear programming problem
- Matlab oe. as useful for prototyping but nt sufficient as HPC kernels
- Julia(/py) is s 'high-level, high-performance, dynamic' programming language. syntax can be close to math notation
  - Faster and easier to ensure correctness
  - Uses external libs like blas, ipopt -> Julia is a simple interface to these libs



### Software Libraries

#### Dense

1. BLAS, 
1. Lapack = BLAS level 3 (mat-mat ops)
1. **P**BLAS, parallel BLAS, 
1. **Sca**Lapack, multi-processing with distributed memory
1. BLA**C**S c for communication, handles inter-process*or* BLAS

#### Sparse Solvers

These are all based on the dense versions

1. MUMPS, $Ax=b$, with $A$ square and unsymmetric, spd orgeneral symmetric
   1. Uses metis
   1. Sequential and parallel (with MPI)