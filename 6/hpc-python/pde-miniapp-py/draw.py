from pde_miniapp_py import viz

# initial
viz.draw("simulation", 0)
viz.plt.show()

# final
viz.draw("simulation", 100)
viz.plt.show()
viz.plt.savefig("py-sim-t100.pdf")


