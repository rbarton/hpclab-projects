from mpi4py import MPI
import numpy as np

# get comm, size & rank
comm = MPI.COMM_WORLD

worldSize = comm.Get_size()
worldRank = comm.Get_rank()

dims = [0, 0]
dims = MPI.Compute_dims(worldSize, dims)
cart = comm.Create_cart(dims, periods=[1, 1], reorder=False)

size = cart.Get_size()
rank = cart.Get_rank()
coord = cart.Get_coords(rank)

west, east = cart.Shift(direction=0, disp=1)
south, north = cart.Shift(direction=1, disp=1)

cart.send(rank, dest=west, tag=0)
re = cart.recv(source=east, tag=0)

cart.send(rank, dest=east, tag=1)
rw = cart.recv(source=west, tag=1)

cart.send(rank, dest=north, tag=2)
rs = cart.recv(source=south, tag=2)

cart.send(rank, dest=south, tag=3)
rn = cart.recv(source=north, tag=3)

print(f"rank {rank}, coord {coord}: n={north}, s={south}, w={west}, e={east}\n"
      f"data                   n={rn}, s={rs}, w={rw}, e={re}")
