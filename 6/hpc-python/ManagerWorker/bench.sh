#!/usr/bin/env bash

nx=8001
csv=data.csv
mkdir -p out/

if [[ -z $csv || ! -s $csv ]]; then
    echo "ranks,   nx,   ny, ntasks, time" > $csv
fi

for p in 2 4 6 8
do
    echo "-- $p ranks"
    for ntasks in 50 100
    do
        echo "$ntasks tasks"
        tmpfile=out/$p-$ntasks.txt

        mpirun -n $p python3 manager_worker.py $nx $nx $ntasks > $tmpfile 2>&1 || exit 1

        # Extract runtime and add csv entry
        tmp=$(cat $tmpfile | grep "^csv: ")
        echo "${tmp#csv: }" >> $csv
    done
done