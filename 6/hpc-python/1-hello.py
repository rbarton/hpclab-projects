from mpi4py import MPI
import numpy as np

# get comm, size & rank
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

# hello
print(f"Hello world from rank {rank} out of {size} processes")

# Slow
sum = comm.allreduce(rank, MPI.SUM)
print(sum)

# Fast with buffers
sendBuf = np.array([rank])
recvBuf = np.array(1)
sum = comm.Allreduce(sendBuf, recvBuf, MPI.SUM)
print(recvBuf)

