#!/usr/bin/env bash

nt=100
tf=0.01
csv=data.csv
mkdir -p out/

if [[ -z $csv || ! -s $csv ]]; then
    echo "ranks,   nx,   nt,   tf, cgIters, newtonIters, cgItersFreq, time" > $csv
fi

for p in 1 2 4 6 8
do
    echo "-- $p ranks"
    for nx in 128 256 512 #1024
    do
        echo "$nx"
        tmpfile=out/$p-$nx.txt

        mpirun -n $p ./main $nx $nt $tf > $tmpfile 2>&1 || exit 1

        # Extract runtime and add csv entry
        tmp=$(cat $tmpfile | grep "^csv: ")
        echo "${tmp#csv: }" >> $csv
    done
done