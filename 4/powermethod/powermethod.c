/****************************************************************
 *                                                              *
 * This file has been written as a sample solution to an        *
 * exercise in a course given at the CSCS-USI Summer School.    *
 * It is made freely available with the understanding that      *
 * every copy of this file must include this header and that    *
 * CSCS/USI take no responsibility for the use of the enclosed  *
 * teaching material.                                           *
 *                                                              *
 * Purpose: : Parallel matrix-vector multiplication and the     *
 *            and power method                                  *
 *                                                              *
 * Contents: C-Source                                           *
 *                                                              *
 ****************************************************************/


#include <stdio.h>
#include <mpi.h>
#include <math.h>
#include <stdlib.h>
#include "hpc-power.h"

double* generate_identity(int n, int startrow, int numrows);
double* generate_ones(int n, int startrow, int numrows);


int main (int argc, char *argv[])
{
    int my_rank, size;
    int i, j;
    double time_start, time_end, time;
    const int n = strtol(argv[1], NULL, 10);
    const int k = strtol(argv[2], NULL, 10);

    MPI_Status  status;
    MPI_Request request;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // create matrix slice A for each process
    int rowsPerProcess = n / size;
    int begin = my_rank * rowsPerProcess;
    int end = begin + rowsPerProcess;

    // double* A = generate_ones(n, begin, rowsPerProcess);
    // double* A = generate_identity(n, begin, rowsPerProcess);
    double* A = hpc_generateMatrix(n, begin, rowsPerProcess);

    double x[n], xNew[rowsPerProcess];
    double normGlobal = 0, normLocal = 0, value;

    // fill x with random values and compute first norm
    // Note: rand seed (srand) is 1 by default, so the same for all ranks
    for (i = 0; i < n; ++i) {
        value = rand();
        x[i] = value;
        normGlobal += value * value;
    }
    normGlobal = sqrt(normGlobal);

    time_start = hpc_timer();
    int t;
    for (t = 0; t < k; ++t) {
        // powerMethod - to estimate the spectral radius of A

        // norm - normalize x
        for (i = 0; i < n; ++i) {
            value = x[i] / normGlobal;
            x[i] = value;
        }

        // matVec - compute new x = A * x
        for (i = 0; i < rowsPerProcess; ++i) {
            value = 0;
            for (j = 0; j < n; ++j) {
                value += A[i * n + j] * x[j];
            }
            xNew[i] = value;
            normLocal += value * value;
        }

        // send local sum and receive global sum (reduction over local sums)
        MPI_Allreduce(&normLocal, &normGlobal, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        normGlobal = sqrt(normGlobal);
        normLocal = 0;
        // send local x elements and receive complete x
        MPI_Allgather(xNew, rowsPerProcess, MPI_DOUBLE, x, rowsPerProcess, MPI_DOUBLE, MPI_COMM_WORLD);
    }
    time_end = hpc_timer();
    time = time_end - time_start;

    if (my_rank == 0) {
        printf("Final result: %05f with runtime of: %05f seconds\n", normGlobal, time);
        printf("Correct? %.d\n", hpc_verify(x, n, time));
        // nodes, ranks, n, k, runtime, resultNorm
        printf("csv %i, %i, %i, %05f, %05f\n", size, n, k, time, normGlobal);
    }


    MPI_Finalize();
    return 0;
}


double* generate_identity(int n, int startrow, int numrows) {
    double* A;
    int i;
    int diag;

    A = (double*)calloc(n * numrows, sizeof(double));

    for (i = 0; i < numrows; i++) {
        diag = startrow + i;

        A[i * n + diag] = 1;
    }

    return A;
}

double* generate_ones(int n, int startrow, int numrows) {
    double* A;
    int i, j;
    int diag;

    A = (double*)calloc(n * numrows, sizeof(double));

    for (i = 0; i < numrows; i++) {
        for (j = 0; j < n; ++j) {
            A[i * n + j] = 1;
        }
    }

    return A;
}
