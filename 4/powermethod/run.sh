#!/bin/bash

mkdir -p out

module load new gcc/6.3.9 open_mpi/3.0.0 python/3.6.0 || :

function setn (){
    sed -i "s/int n \=.*/int n \= ${1}\;/" powermethod.c
}
function setk (){
    sed -i "s/int k \=.*/int k \= ${1}\;/" powermethod.c
}

n=4800
k=1000
make || exit 1


echo "--- Strong Scaling ---"

for nodes in 1 2 3 4
do
    echo "-- $nodes node/s"
    for p in 1 4 8 12 16 32 64
    do
        ptile=$(($p / $nodes))
        if (( $p % $nodes != 0 || $ptile > 24 )); then
            continue
        fi

        echo "- $p/$ptile"

        # mpirun -n $p ./powermethod $n $k > out/s$p-$nodes.txt
        bsub -n $p -R "span[ptile=$ptile]" -R "select[model==XeonE5_2680v3]" -W 01:00 \
        -oo out/s$p-$nodes.txt mpirun ./powermethod $n $k
    done
done


echo ""
echo "--- Weak Scaling ---"

n0=14400
k=100

for nodes in 1 2 3 4
do
    echo "-- $nodes node/s"
    for p in 1 4 8 12 16 32 64
    do
        ptile=$(($p / $nodes))
        if (( $p % $nodes != 0 || $ptile > 24 )); then
            continue
        fi

        n=$(python3 get-n.py $p $n0)
        
        echo "- $p/$ptile n=$n"

        # mpirun -n $p ./powermethod $n $k > out/w$p-$nodes.txt
        bsub -n $p -R "span[ptile=$ptile]" -R "select[model==XeonE5_2680v3]" -W 01:00 \
        -oo out/w$p-$nodes.txt mpirun ./powermethod $n $k
    done
done