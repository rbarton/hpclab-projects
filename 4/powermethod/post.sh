#!/bin/bash

csv=data.csv

if [[ -z $csv || ! -s $csv ]]; then
    echo "scaling, nodes, ranks, n, k, time, resultNorm" > $csv
fi

for nodes in 1 2 3 4
do
    echo "-- $nodes node/s"
    for p in 1 4 8 12 16 32 64
    do
        ptile=$(($p / $nodes))
        if (( $p % $nodes != 0 || $ptile > 24 )); then
            continue
        fi

        echo "- $p/$ptile"

        echo -n "s, $nodes, " >> $csv
        # tail -1 out/s$p-$nodes.txt >> $csv
        tmp=$(cat out/s$p-$nodes.txt | grep "^csv ")
        echo ${tmp#csv } >> $csv

        echo -n "w, $nodes, " >> $csv
        # tail -1 out/w$p-$nodes.txt >> $csv
        tmp=$(cat out/w$p-$nodes.txt | grep "^csv ")
        echo ${tmp#csv } >> $csv

    done
done