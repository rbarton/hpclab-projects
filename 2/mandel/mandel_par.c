#include <stdlib.h>
#include <stdio.h>

#include <unistd.h>
#include <time.h>
#include <sys/time.h>

#include "pngwriter.h"
#include "consts.h"
#include <omp.h>

unsigned long get_time ()
{
    struct timeval tp;
    gettimeofday (&tp, NULL);
    return tp.tv_sec * 1000000 + tp.tv_usec;
}

int main (int argc, char** argv)
{
	png_data* pPng = png_create (IMAGE_WIDTH, IMAGE_HEIGHT);
	
	double x, y, x2, y2, cx, cy;

	const double fDeltaX = (MAX_X - MIN_X) / (double) IMAGE_WIDTH;
	const double fDeltaY = (MAX_Y - MIN_Y) / (double) IMAGE_HEIGHT;
	
	long nTotalIterationsCount = 0;
	unsigned long nTimeStart = get_time ();
	
	long i, j, n;
    
	const int chunkSize = 2;
	// do the calculation
	for (j = 0; j < IMAGE_HEIGHT; j++)
	{
		cy = MIN_Y + j * fDeltaY;
		cx = MIN_X;
		#pragma omp parallel for schedule(dynamic, chunkSize) reduction(+:nTotalIterationsCount) \
		private(x, y, x2, y2, cx, n, i)
		for (i = 0; i < IMAGE_WIDTH; i++)
		{
			x = cx;
			y = cy;
			x2 = x * x;
			y2 = y * y;
			// compute the orbit z, f(z), f^2(z), f^3(z), ...
			// count the iterations until the orbit leaves the circle |z|=2.
			// stop if the number of iterations exceeds the bound MAX_ITERS.

			n = 0;
			while(n < MAX_ITERS && x2 + y2 < 2){
				y = 2 * x * y + cy;
				x = x2 - y2 + cx;
				x2 = x * x;
				y2 = y * y;
				++n;
			}
			nTotalIterationsCount += n;

			// n indicates if the point belongs to the mandelbrot set
			// plot the number of iterations at point (i, j)
			int c = ((long) n * 255) / MAX_ITERS;
			png_plot (pPng, i, j, c, c, c);
			cx = MIN_X + i * fDeltaX;
		}
	}
	unsigned long nTimeEnd = get_time ();
	
	// print benchmark data
	printf ("Total time:                 %g millisconds\n", (nTimeEnd - nTimeStart) / 1000.0);
	printf ("Image size:                 %ld x %ld = %ld Pixels\n",
		(long) IMAGE_WIDTH, (long) IMAGE_HEIGHT, (long) (IMAGE_WIDTH * IMAGE_HEIGHT));
	printf ("Total number of iterations: %ld\n", nTotalIterationsCount);
	printf ("Avg. time per pixel:        %g microseconds\n", (nTimeEnd - nTimeStart) / (double) (IMAGE_WIDTH * IMAGE_HEIGHT));
	printf ("Avg. time per iteration:    %g microseconds\n", (nTimeEnd - nTimeStart) / (double) nTotalIterationsCount);
	printf ("Iterations/second:          %g\n", nTotalIterationsCount / (double) (nTimeEnd - nTimeStart) * 1e6);
	// assume there are 8 floating point operations per iteration
	printf ("MFlop/s:                    %g\n", nTotalIterationsCount * 8.0 / (double) (nTimeEnd - nTimeStart));

	//method, cores, chunk-size, image-width, pixels, time, pixel-time, iter-time, iter-freq, Mflops
	printf("par, %i, %i, %ld, %ld, %g, %g, %g, %g, %g\n"
	, omp_get_max_threads()
	, chunkSize
	, (long) IMAGE_WIDTH
	, (long) (IMAGE_WIDTH * IMAGE_HEIGHT)
	, (nTimeEnd - nTimeStart) / 1000.0
	, (nTimeEnd - nTimeStart) / (double) (IMAGE_WIDTH * IMAGE_HEIGHT)
	, (nTimeEnd - nTimeStart) / (double) nTotalIterationsCount
	, nTotalIterationsCount / (double) (nTimeEnd - nTimeStart) * 1e6
	, nTotalIterationsCount * 8.0 / (double) (nTimeEnd - nTimeStart));

	png_write (pPng, "mandel.png");
	return 0;
}
