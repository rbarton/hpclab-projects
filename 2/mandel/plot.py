#!/usr/bin/python3
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

data = pd.read_csv("out/data.csv")
sns.set_style("whitegrid")

ax1 = sns.boxplot(data=data, x=data['width'], y=data['mflops'], hue=data['#cores'],)

plt.savefig("weak-scaling.pdf", bbox_inches='tight')