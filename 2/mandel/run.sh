#!/usr/bin/env bash

img="mandel.png"
csv=out/data.csv
mkdir -p out
if [[ -z $csv || ! -s $csv ]]; then
    echo "method, cores, chunk-size, image-width, pixels, time, pixel-time, iter-time, iter-freq, Mflops" > $csv
fi

make || exit 1

for dim in 512 #1024 2048 4096
do
    echo "--$dim"
    sed -i "s/^\#define IMAGE_WIDTH   .*/#define IMAGE_WIDTH   ${dim}/g" consts.h
    sed -i "s/^\#define IMAGE_HEIGHT  .*/#define IMAGE_HEIGHT  ${dim}/g" consts.h
    
    make

    ./mandel_seq > out/seq${dim}.txt
    mv $img out/seq${dim}.png 
    tail -1 out/seq${dim}.txt >> $csv

    for i in 1 {4..16..4}
    do
        echo "- $i"
        OMP_NUM_THREADS=$i ./mandel_par > out/par${dim}.txt
        mv $img out/par${dim}.png
        tail -1 out/par${dim}.txt >> $csv
    done
done
