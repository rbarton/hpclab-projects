#!/bin/bash

csv=out/data.csv
mkdir -p out
if [[ -z $csv || ! -s $csv ]]; then
    echo "cores,method,N,time,iters" > $csv
fi

for N in 100000 1000000 10000000 100000000 1000000000
do
    sed -i "s/\#define N .*/\#define N $N/" dotProduct.cpp
    echo "-- $N"

    make
    for i in 1 {2..24..2}
    do
        echo "- $i"
        OMP_NUM_THREADS=$i ./dotProduct > out/$i.txt
        tail -3 out/$i.txt >> $csv
    done
done
