//make && ./dotProduct >> out.txt
#include <iostream>
#include "walltime.h"
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include <omp.h>

#define NUM_ITERATIONS 100

// Example benchmarks
// 0.008s ~0.8MB (100k)
#define N 100000
// 0.1s ~8MB (1m)
// #define N 1000000
// 1.1s ~80MB (10m)
// #define N 10000000
// 13s ~800MB (100m)
// #define N 100000000
// 127s 16GB (1b)
//#define N 1000000000
#define EPSILON 0.1


using namespace std;

int main()
{
    cout << "threads = " << omp_get_max_threads() << endl;
    cout << "N = " << N << endl;

    int myId, numTdreads;
    double time_serial, time_start = 0.0;
    long double dotProduct;
    double *a,*b;

    // Allocate memory for the vectors as 1-D arrays
    a = new double[N];
    b = new double[N];
  
    // Initialize the vectors with some values
    for(int i=0; i<N; i++) {
        a[i] = i;
        b[i] = i/10.0;
    }

    long double alpha = 0;
    // serial execution
    // Note that we do extra iterations to reduce relative timing overhead
    time_start = wall_time();
    for(int iterations=0; iterations<NUM_ITERATIONS; iterations++) {
        alpha=0.0;
        for( int i=0; i<N; i ++) {
            alpha += a[i] * b[i];
    	}
    }
    time_serial = wall_time() - time_start;
    cout << "Serial execution time      = " << time_serial << " sec" << endl;
  
    long double alpha_parallel = 0;
    double time_red = 0;
    double time_critical = 0;

    //   TODO: Write parallel version (2 ways!)
    //   i.  Using reduction pragma
    time_start = wall_time();
    for(int iterations=0; iterations<NUM_ITERATIONS; iterations++) {
        alpha_parallel=0.0;
        #pragma omp parallel for reduction(+:alpha_parallel)
        for( int i=0; i<N; i ++) {
            alpha_parallel += a[i] * b[i];
        }
    }
    time_red = wall_time() - time_start;
    cout << "Parallel i execution time  = " << time_red << " sec" << endl;


    //   ii. Using  critical pragma
    time_start = wall_time();
    if(N <= 1000000)
        for(int iterations=0; iterations<NUM_ITERATIONS; iterations++) {
            alpha_parallel=0.0;
            #pragma omp parallel for
            for( int i=0; i<N; i ++) {
                #pragma omp critical
                alpha_parallel += a[i] * b[i];
            }
        }
    time_critical = wall_time() - time_start;
    cout << "Parallel ii execution time = " << time_critical << " sec" << endl;




    if( (fabs(alpha_parallel - alpha)/fabs(alpha_parallel)) > EPSILON) {
        cout << "parallel reduction: " << alpha_parallel << " serial :" << alpha << "\n";
        cerr << "Alpha not yet implemented correctly!\n";
        exit(1);
    }
    cout << "Parallel dot product = " << alpha_parallel
	 << " time using reduction method = " << time_red
	 << " sec, time using critical method " << time_critical
	 << " sec" << endl
     << "Speedup seq:para1:para2 = 1 : " << time_serial/time_red << " : " << time_serial/time_critical
     << "\nThreads = " << omp_get_max_threads()
     << endl << endl
     << omp_get_max_threads() << ", " << "Serial   , " << N << ", " << time_serial   << ", " << NUM_ITERATIONS << endl
     << omp_get_max_threads() << ", " << "Reduction, " << N << ", " << time_red      << ", " << NUM_ITERATIONS <<endl;
    if(N <= 1000000)
        cout << omp_get_max_threads() << ", " << "Critical , " << N << ", " << time_critical << ", " << NUM_ITERATIONS << endl;
     
  
    // De-allocate memory
    delete [] a;
    delete [] b;

    return 0;
}
