# include <stdlib.h>
# include <math.h>
# include "walltime.h"

int main ( int argc, char *argv[] ) {
    int N = 2000000000;
    const double up = 1.00000001;
    double Sn = up;
    int n;
    /* allocate memory for the recursion */
    double* opt = (double*) malloc ((N+1)* sizeof(double));

    if (opt == NULL)  die ("failed to allocate problem size");

    double time_start = wall_time();
    int first = 1;
    #pragma omp parallel for shared(opt) private(n, Sn) firstprivate(first) schedule(static)
    for (n = 0; n <= N; ++n) {
        if(first != 0){
            first = 0;
            Sn = pow(up, n + 1); //potentially execute pre-for loop to prevent if's inside, first n is required
        }
        opt[n] = Sn;
        Sn *= up;
    }
    //Could also unroll the loop to not evaluate the if on every iteration, but would require extra checks

	Sn = opt[N];

    double time = wall_time()- time_start;

	printf("up:%d\n", up);
    printf("Parallel RunTime   :  %f seconds\n", wall_time()- time_start);
    printf("Final Result Sn    :  %e \n", Sn );

    // double temp = 0.0;
    // for (n = 0; n <= N; ++n) {
	//     temp +=  opt[n] * opt[n];
    // }
    // printf("Result ||opt||^2_2 :  %1.12f\n", temp/(double) N);
    // printf ( "\n" );
    
    printf("par-stat, %i, %f\n"
        , omp_get_max_threads()
        , time);

    return 0;
}
