# include <stdlib.h>
# include <math.h>
# include "walltime.h"

int main ( int argc, char *argv[] ) {
    int N = 2000000000;
    const double up = 1.00000001;
    double Sn = up;
    int n;
    /* allocate memory for the recursion */
    double* opt = (double*) malloc ((N+1)* sizeof(double));

    if (opt == NULL)  die ("failed to allocate problem size");

    double time_start = wall_time();
    
    //if i+j=n   => opt[n] = opt[i] * opt[j]
    //opt[n-1] = Sn^n

    //Fill in powers of 2 sequentially in O(log(n))
    for (unsigned int n = 1; n <= N +1; n *= 2) {
        opt[n -1] = Sn;
        Sn *= Sn;
    }

//	const int chunk = 1024* 8;
	const unsigned int chunk = 1024*1024 *8; //use a sufficiently large chunk size
	unsigned int a;
    #pragma omp parallel for default(shared) private(n, Sn, a) schedule(dynamic,chunk)
	for (n = 0; n <= N; ++n) {
        // Calculate Sn = pow(up, n + 1) in O(log(n))
        if(n % chunk == 0) //if at the beginning of a chunk
        {
            // Get up^n+1 by decomposing n into binary
            a = 1;
            Sn = 1;
            while(a <= n){
                if((n & a) != 0)
                    Sn *= opt[a-1];
                a *= 2;
            }
            // Sn = pow(up, n+1); //alternative
        }

        opt[n] = Sn;
        Sn *= up;
    }

	Sn = opt[N];


    double time = wall_time()- time_start;

	printf("up:%g\n", up);
    printf("Parallel RunTime   :  %f seconds\n", wall_time()- time_start);
    printf("Final Result Sn    :  %e \n", Sn );

    // double temp = 0.0;
    // for (n = 0; n <= N; ++n) {
	//     temp +=  opt[n] * opt[n];
    // }
    // printf("Result ||opt||^2_2 :  %1.12f\n", temp/(double) N);
    
    printf ( "\n" );
    printf("par-dyn, %i, %f\n"
        , omp_get_max_threads()
        , time);
    return 0;
}
