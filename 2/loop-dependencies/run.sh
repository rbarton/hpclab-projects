#!/bin/bash
# module load new gcc/6.3.0
# make
# bsub -n 16 -W 00:10 -R "rusage[mem=2048]" -R "span[ptile=16]" ./recur_seq
# export OMP_NUM_THREADS=16
# bsub -n 16 -W 00:10 -R "rusage[mem=2048]" -R "span[ptile=16]" ./recur_dyn

csv=out/data.csv
mkdir -p out
if [[ -z $csv || ! -s $csv ]]; then
    echo "method, cores, time" > $csv
fi
make

./recur_seq > out/tmp.txt
tail -1 out/tmp.txt >> $csv

for i in 1 {4..16..4}
do
    echo "- $i"
    OMP_NUM_THREADS=$i ./recur_dyn > out/tmp$i.txt
    tail -1 out/tmp$i.txt >> $csv
    OMP_NUM_THREADS=$i ./recur_stat >> out/tmp$i.txt
    tail -1 out/tmp$i.txt >> $csv
done
