function [cut_recursive,cut_kway] = Bench_metis(picture)
% Compare recursive bisection and direct k-way partitioning,
% as implemented in the Metis 5.0.2 library.

%  Add necessary paths
addpaths_GP;
showFigs = 1;

% Note: Mostly copied from Bench_rec_bisection.m
% Graphs in question
cases = {
    'usroads.mat';
    'luxembourg_osm.mat';
%     'barth4.mat';
%     'mesh3e1.mat';
%     'crack.mat';
    };

nc = length(cases);
maxlen = 0;
for c = 1:nc
    if length(cases{c}) > maxlen
        maxlen = length(cases{c});
    end
end

for c = 1:nc
    fprintf('.');
    sparse_matrices(c) = load(cases{c});
end


fprintf('\n\n Report Cases         Nodes     Edges\n');
fprintf(repmat('-', 1, 40));
fprintf('\n');
for c = 1:nc
    spacers  = repmat('.', 1, maxlen+3-length(cases{c}));
    [params] = Initialize_case(sparse_matrices(c));
    fprintf('%s %s %10d %10d\n', cases{c}, spacers,params.numberOfVertices,params.numberOfEdges);
end

fprintf('\n%15s %22s %20s\n','Bisection','Recursive','k-way');
fprintf('%20s %10d %10d %10d %10d\n','Partitions',16,32,16,32);
fprintf(repmat('-', 1, 80));
fprintf('\n');

for c = 1:nc
    spacers = repmat('.', 1, maxlen+3-length(cases{c}));
    fprintf('%s %s', cases{c}, spacers);
    sparse_matrix = load(cases{c});
    
    % Initialize the problem
    [params] = Initialize_case(sparse_matrices(c));
    W      = params.Adj;
    coords = params.coords;   
    
    % Steps
    % 1. Initialize the cases

    % 2. Call metismex to
    %     a) Recursively partition the graphs in 16 and 32 subsets.
    [mapRec16, sepij,sepA] = rec_bisection('bisection_metis', 4, W, coords, 0);
    [mapRec32, sepij,sepA] = rec_bisection('bisection_metis', 5, W, coords, 0);
    rec8 = cutsize(W,mapRec16);
    rec16 = cutsize(W,mapRec32);
    
    %     b) Perform direct k-way partitioning of the graphs in 16 and 32 subsets.
    [mapKway16, edgecut] = metismex('PartGraphKway',W,16);
    [mapKway32, edgecut] = metismex('PartGraphKway',W,32);
    kway8 = cutsize(W,mapKway16);
    kway16 = cutsize(W,mapKway32);


    fprintf('%10d %10d %10d %10d\n', rec8, rec16, kway8, kway16);
    
    % 3. Visualize the results for 32 partitions
    if showFigs
        clf reset
        gplotmap(W, coords, mapRec16);
        title('Rec Bisection - Metis 16 parts')
        pause;
       
        clf reset
        gplotmap(W, coords, mapRec32);
        title('Rec Bisection - Metis 32 parts')
        pause;

        clf reset
        gplotmap(W, coords, mapKway16);
        title('Direct k-way Bisection - Metis 16 parts')
        pause;

        clf reset
        gplotmap(W, coords, mapKway32);
        title('Direct k-way Bisection - Metis 32 parts')
        pause;
    end
end
