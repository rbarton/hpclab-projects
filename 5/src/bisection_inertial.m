function [part1,part2] = bisection_inertial(A,xy,picture)
% INERTPART : Inertial partition of a graph.
%
% p = inertpart(A,xy) returns a list of the vertices on one side of a partition
%     obtained by bisection with a line or plane normal to a moment of inertia
%     of the vertices, considered as points in Euclidean space.
%     Input A is the adjacency matrix of the mesh (used only for the picture!);
%     each row of xy is the coordinates of a point in d-space.
%
% inertpart(A,xy,1) also draws a picture.
%
% See also PARTITION


% Steps
% 1. Calculate the center of mass.
xbar = mean(xy);

% 2. Construct the matrix M.
%  (Consult the pdf of the assignment for the creation of M) 
M = cov(xy);

% 3. Calculate the smallest eigenvector of M.  
[V,D] = eig(M);
u = V(:,2);

% 4. Find the line L on which the center of mass lies.
% As L = xbar + alpha*u 
% we just remove the xbar from xy and use partition with u

% 5. Partition the points around the line L.
%   (you may use the function partition.m)
part1 = partition(xy - xbar, u);
part2 = other(part1,A);

if picture == 1
    gplotpart(A,xy,part1);
    title('Inertial bisection');
end

end