% Benchmark for recursively partitioning meshes, based on various
% bisection approaches
%
% D.P & O.S for HPC2020 in ETH



% add necessary paths
addpaths_GP;
nlevels_a = 3;
nlevels_b = 4;
showFigs = 1;

fprintf('       *********************************************\n')
fprintf('       ***  Recursive graph bisection benchmark  ***\n');
fprintf('       *********************************************\n')

% load cases
cases = {
    'airfoil1.mat';
    '3elt.mat';
    'barth4.mat';
    'mesh3e1.mat';
    'crack.mat';
    };

nc = length(cases);
maxlen = 0;
for c = 1:nc
    if length(cases{c}) > maxlen
        maxlen = length(cases{c});
    end
end

for c = 1:nc
    fprintf('.');
    sparse_matrices(c) = load(cases{c});
end


fprintf('\n\n Report Cases         Nodes     Edges\n');
fprintf(repmat('-', 1, 40));
fprintf('\n');
for c = 1:nc
    spacers  = repmat('.', 1, maxlen+3-length(cases{c}));
    [params] = Initialize_case(sparse_matrices(c));
    fprintf('%s %s %10d %10d\n', cases{c}, spacers,params.numberOfVertices,params.numberOfEdges);
end

%% Create results table
fprintf('\n%7s %16s %20s %16s %16s\n','Bisection','Spectral','Metis 5.0.2','Coordinate','Inertial');
fprintf('%10s %10d %6d %10d %6d %10d %6d %10d %6d\n','Partitions',8,16,8,16,8,16,8,16);
fprintf(repmat('-', 1, 100));
fprintf('\n');


for c = 1:nc
    spacers = repmat('.', 1, maxlen+3-length(cases{c}));
    fprintf('%s %s', cases{c}, spacers);
    sparse_matrix = load(cases{c});
    

    % Recursively bisect the loaded graphs in 8 and 16 subgraphs.
    % Steps
    % 1. Initialize the problem
    [params] = Initialize_case(sparse_matrices(c));
    W      = params.Adj;
    coords = params.coords;    
    % 2. Recursive routines
    % i. Spectral    
    [mapSpec8, sepij,sepA] = rec_bisection('bisection_spectral', nlevels_a, W, coords, 0);
    [mapSpec16, sepij,sepA] = rec_bisection('bisection_spectral', nlevels_b, W, coords, 0);
    
    
    % ii. Metis
    [mapMetis8, sepij,sepA] = rec_bisection('bisection_metis', nlevels_a, W, coords, 0);
    [mapMetis16, sepij,sepA] = rec_bisection('bisection_metis', nlevels_b, W, coords, 0);
    
    
    % iii. Coordinate    
    [mapCoord8, sepij,sepA] = rec_bisection('bisection_coordinate', nlevels_a, W, coords, 0);
    [mapCoord16, sepij,sepA] = rec_bisection('bisection_coordinate', nlevels_b, W, coords, 0);
    
    % iv. Inertial
    [mapInert8, sepij,sepA] = rec_bisection('bisection_inertial', nlevels_a, W, coords, 0);
    [mapInert16, sepij,sepA] = rec_bisection('bisection_inertial', nlevels_b, W, coords, 0);
    
    
    % 3. Calculate number of cut edges
    spectral8 = cutsize(W,mapSpec8);
    spectral16 = cutsize(W,mapSpec16);
    
    metis8 = cutsize(W,mapMetis8);
    metis16 = cutsize(W,mapMetis16);
    
    coordinate8 = cutsize(W,mapCoord8);
    coordinate16 = cutsize(W,mapCoord16);
    
    inertial8 = cutsize(W,mapInert8);
    inertial16 = cutsize(W,mapInert16);
    
    
    fprintf('%6d %6d %10d %6d %10d %6d %10d %6d\n', spectral8, spectral16,...
    metis8, metis16, coordinate8, coordinate16, inertial8, inertial16);
    
    % 4. Visualize the partitioning result
    if showFigs
        clf reset
        gplotmap(W, coords, mapSpec8);
        title('Rec Bisection - Spectral 8 parts')
        pause;

        clf reset
        gplotmap(W, coords, mapSpec16);
        title('Rec Bisection - Spectral 16 parts')
        pause;

        clf reset
        gplotmap(W, coords, mapMetis8);
        title('Rec Bisection - Metis 8 parts')
        pause;

        clf reset
        gplotmap(W, coords, mapMetis16);
        title('Rec Bisection - Metis 16 parts')
        pause;

        clf reset
        gplotmap(W, coords, mapCoord8);
        title('Rec Bisection - Coordinate 8 parts')
        pause;

        clf reset
        gplotmap(W, coords, mapCoord16);
        title('Rec Bisection - Coordinate 16 parts')
        pause;

        clf reset
        gplotmap(W, coords, mapInert8);
        title('Rec Bisection - Inertial 8 parts')
        pause;

        clf reset
        gplotmap(W, coords, mapInert16);
        title('Rec Bisection - Inertial 16 parts')
        pause;
    end
    
end