% Visualize information from the eigenspectrum of the graph Laplacian
%
% D.P & O.S for HPC2020 in ETH

% add necessary paths
addpaths_GP;

% Graphical output at bisection level
picture = 0;

% Cases under consideration
load airfoil1.mat;
% load 3elt.mat;
% load barth4.mat;
% load mesh3e1.mat;
% load crack.mat;

% Initialize the cases
W      = Problem.A;
coords = Problem.aux.coord;

% Steps
% 1. Construct the graph Laplacian of the graph in question.
L = -W + diag(sum(W,1));

% 2. Compute eigenvectors associated with the smallest eigenvalues.
[V,D] = eigs(L,3,'smallestabs');
v2 = V(:,2);
v3 = V(:,3);

% 3. Perform spectral bisection.
p = v2 > median(v2);
[part1, part2] = other(p);

% 4. Visualize:
%   i.   The first and second eigenvectors.
xy = V(:,1:2);
clf reset;
bar(V(:,1))
hold on;
bar(V(:,2))
% bar(coords(:,1))
hold off;
pause;

%   ii.  The second eigenvector projected on the coordinate system space of graphs.
xyz = [coords, zeros(size(coords, 1))];
clf reset;
gplotpart(W, xyz, part1);
hold on;
title('Fiedler eigenvector')

scatter3(coords(:,1), coords(:,2), v2, 10, v2, 'filled');
zlim([min(v2) max(v2)])
colorbar();
grid on
hold off;
pause;

%   iii. The spectral bi-partitioning results using the spectral coordinates of each graph.

coords_eig = V(:,2:3);
clf reset;
gplotpart(W, coords, part1);
title('Real coords')
pause;

clf reset;
gplotpart(W, coords_eig, part1);
title('Eigen coords')
pause;


