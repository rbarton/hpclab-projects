/* 
    Please include compiler name below (you may also include any other modules you would like to be loaded)

COMPILER= gnu

    Please include All compiler flags and libraries as you want them run. You can simply copy this over from the Makefile's first few lines
 
CC = cc
OPT = -O3
CFLAGS = -Wall -std=gnu99 $(OPT)
MKLROOT = /opt/intel/composer_xe_2013.1.117/mkl
LDLIBS = -lrt -Wl,--start-group $(MKLROOT)/lib/intel64/libmkl_intel_lp64.a $(MKLROOT)/lib/intel64/libmkl_sequential.a $(MKLROOT)/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread -lm

*/
#include <immintrin.h>
#include <math.h>

#define MIN(a,b) a < b ? a : b
const char* dgemm_desc = "Naive, three-loop dgemm.";

/* This routine performs a dgemm operation
 *  C := C + A * B
 * where A, B, and C are lda-by-lda matrices stored in column-major format.
 * On exit, A and B maintain their input values.
 * 
 * Approach: 
 * Do power of 2 blocks first, then do the edges*/
void square_dgemm (int n, double* A, double* B, double* C)
{
  const int s = 12; //from s <= sqrt(M/3), using L1=32kB, divisible by 4 for AVX
  const int blocks = n/s; //round up integer division
  const int totalBlocks = (n + s/2) /s;
  double tmp4[4] __attribute__ ((aligned(64)));

    for (int b = 0; b < n; b += s)  //j of block
  for (int a = 0; a < n; a += s)    //i of block
    {
      const int COffset = a + b*n;       //index in C
      for (int c = 0; c < n; c += s){
        const int AOffset = a + c*n;
        const int BOffset = c + b*n;

        // NaiveMM inside s-by-s block
        const int sI = MIN(s, n - a);
        const int sJ = MIN(s, n - b);
        const int sK = MIN(s, n - c);
          for (int j = 0; j < sJ; ++j)
        for (int i = 0; i < sI; ++i)
          {
            double cij = C[COffset + i+j*n];

            if(sK == s){
              int aik = AOffset + i; //first element in RowVector4
              for(int k = 0; k < sK; k += 4){
                __m256d a = _mm256_set_pd(A[aik+3*n], A[aik+2*n], A[aik+n], A[aik]);
                __m256d b = _mm256_load_pd(B + BOffset + k+j*n);

                __m256d ab = _mm256_mul_pd(a, b);
                // ab = _mm256_hadd_pd(ab, ab);
                _mm256_store_pd(&tmp4[0], ab);

                // cij += tmp4[0] + tmp4[2];
                cij += tmp4[0] + tmp4[1] + tmp4[2] + tmp4[3];
                aik += n;
              }
              C[COffset + i+j*n] = cij;
            }
            else
            {
              for(int k = 0; k < sK; k++)
                cij += A[AOffset + i+k*n] * 
                       B[BOffset + k+j*n];
                      //block offset + offset inside block
              C[COffset + i+j*n] = cij;
            }
          }
      }
    }
}
