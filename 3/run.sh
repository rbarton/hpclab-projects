#!/usr/bin/env bash

hpcArgs="100 0.01"

csv=out/data.csv
mkdir -p out
if [[ -z $csv || ! -s $csv ]]; then
    echo "method, cores, nx, dx, T, dt, time, cg-iters, cg-iters-freq, newton-iters" > $csv
fi

(cd seq && make) || exit 1
(cd omp && make) || exit 1
(cd avx && make) || exit 1

for dim in 64 128 256 512 1024 #2048 
do
    echo "---$dim---"
    
    echo "- seq"
    ./seq/main $dim $hpcArgs> out/seq${dim}.txt
    tail -1 out/seq${dim}.txt >> $csv

    for i in 1 {4.24..4}
    do
        echo "- omp $i"
        OMP_NUM_THREADS=$i ./omp/main $dim $hpcArgs > out/par$i-${dim}.txt && \
        tail -1 out/par$i-${dim}.txt >> $csv

        # echo "- avx $i"
        # OMP_NUM_THREADS=$i ./avx/main $dim $hpcArgs > out/avx$i-${dim}.txt && \
        # tail -1 out/avx$i-${dim}.txt >> $csv
    done

    echo ""
done
